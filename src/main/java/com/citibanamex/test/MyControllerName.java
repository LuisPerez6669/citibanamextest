package com.citibanamex.test;

import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
public class MyControllerName {

    @GetMapping("/extractHeaders")
    public Map<String, List<String>> extractHeaders(@RequestHeader MultiValueMap<String, String> headers) {
        return headers.entrySet().stream()
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @GetMapping("/getHeaders")
    public Map<String, List<String>> getHeaders(@RequestHeader MultiValueMap<String, String> headers, @RequestBody List<String> keyValues) {
        return headers.entrySet().stream()
            .filter(x -> keyValues.contains(x.getKey()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
