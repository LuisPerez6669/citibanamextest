CitiBanamex Test Luis Enrique Perez Galindo

La aplicacion fue desarrollada en spring boot para tratar de simular el ambiente, para iniciarlo se importa en cualquier IDE (STS, intellij), y se ejecuta. por default se encuentra en el puerto 8080.


Aplicacion se encuentra deployada en aws elastic beanstalk:

http://springboot-demo-citi.us-east-2.elasticbeanstalk.com


Endpoints:

Obtener todos los headers: GET

http://springboot-demo-citi.us-east-2.elasticbeanstalk.com/api/extractHeaders




Obtener headers especificos: GET

http://springboot-demo-citi.us-east-2.elasticbeanstalk.com/api/getHeaders

    Body: ["headertest", "connection"]


Coleccion de postman:

https://www.getpostman.com/collections/d9754971ce26dfa83d34




