package com.citibanamex.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TestApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testHeaders() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", String.valueOf(MediaType.APPLICATION_JSON));
        this.mockMvc.perform(get("/api/extractHeaders").headers(headers)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void getHeaders() throws Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.add("testHeader","hola");
        headers.add("testHeader","mundo");

        List<String> getHeaders = new ArrayList<>();
        getHeaders.add("testHeader");

      this.mockMvc.perform(
            get("/api/extractHeaders")
                .headers(headers)
                .content(getHeaders.toString()))
            .andExpect(content().json("{\"testHeader\":[\"hola\",\"mundo\"],\"Content-Length\":[\"12\"]}"));
    }
}
